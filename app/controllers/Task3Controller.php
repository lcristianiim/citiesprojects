<?php
	class Task3Controller extends BaseController {

		public function __construct()
			{
				parent::__construct();

			}

		public function task_3(){
			// dd($this->general_variables);


            $number_of_cities = DB::table('cities')->count();
            $number_of_citizens = DB::table('citizens')->count();
            $number_of_projects = DB::table('projects')->count();

            return View::make('task_3_layout')->with(array(
                'tasks_presentation' => $this->tasks_presentation,
                'number_of_cities' => $number_of_cities,
                'number_of_citizens' => $number_of_citizens,
                'number_of_projects' => $number_of_projects
            ));
		}

        public function ajax_post(){

            $input = Input::all();

            $input = trim($input['search_bar']);

            $search = DB::table('citizens')
                            ->where('fname', 'like', "%{$input}%")
                            ->orWhere('lname', 'like', "%{$input}%")
                            ->orWhere('address', 'like', "%{$input}%")
                            ->take(100)
                            ->get();

            if (!empty($search)) {
                echo json_encode(array('error' => 0, 'result' => $search));
            } else {
                echo json_encode(array('error' => 1, 'error_message' => "Nu a fost gasita nici o inregistrare dupa criteriul cautat."));
            };
        }

	}
?>
