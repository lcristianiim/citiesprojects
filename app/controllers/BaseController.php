<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	public $cities = [];
	public $cities_list = array();

	public $citizens = [];
	public $citizens_list = array();

    public $projects = [];
    public $projects_list =[];

	public $general_variables = [];

    public $tasks_presentation = [
            [
                'heading' => 'Task 1',
                'paragraph'=> 'Sa se implementeze un script dinamic care la rulare sa populeze cele 3 tablele cu date ce corespund tipurilor predefinite (city, citizen, project). Numarul de orase sa fie de minim 10. Numarul de locuitori de minim 10.000 per oras, iar numarul de proiecte de minim 3 per locuitor (atentie – pe un anumit proiect pot fi asociati mai multi locuitori).',
                'link' => 'task_1'
            ],
            [
                'heading' => 'Task 2',
                'paragraph'=> 'Sa se implementeze o aplicatie care sa permita urmatoarea functionalitate: adaugarea de noi orase, locuitori si asocierea de proiecte anumitor locuitori afisarea tuturor acestor date; pentru fiecare oras sa se afiseze umarul total de locuitori, numarul total de proiecte si numarul de locuitori / km patrat stergerea si editarea datelor existente (city,citizen,project).',
                'link' => 'task_2'
            ],
            [
                'heading' => 'Task 3',
                'paragraph'=> 'Sa se realizeze un script care sa permita cautarea unui locuitor in toate orasele existente functie de nume (lname) sau prenume (fname) sau adresa (address). Sa exista doar un singur camp HTML care sa permita introducerea expresiei de cautare. Rezultatul va fi returnat in format JSON si va fi interpretat client-side din javascript si afisate rezultatele in pagina. Este important timpul de cautare. Se permite folosirea oricarei librarii javascript. Preferabil jQuery.',
                'link' => 'task_3'
            ],
            [
                'heading' => 'Task 4',
                'paragraph'=> 'Sa se realizeze un script care sa trimita cate un e-mail fiecarui locuitor asociat la un anumit proiect, in cazul in care acest proiect a expirat.',
                'link' => 'task_4'
            ],
            [
                'heading' => 'Task 5',
                'paragraph'=> 'Sa se implementeze un serviciu web de tip JSON (service.php) care la accesarea acestuia cu un parametru (id=x) sa returneze datele de contact ale unui locuitor in format json.',
                'link' => 'task_5'
            ],

        ];

	public function __construct()
	{
//		// cities list
//		$this->cities = City::all()->toArray();
//
//		foreach ($this->cities as $city) {
//			$this->cities_list[$city['id']] = $city['name'];
//		};
//
//		// citizens list
//		$this->citizens = Citizen::all()->toArray();
//		foreach ($this->citizens as $citizen) {
//			$this->citizens_list[$citizen['id']] = $citizen['lname'];
//		}
//
//        //projects list
//        $this->projects = Project::all()->toArray();
//        foreach ($this->projects as $project) {
//            $this->projects_list[$project['id']] = $project['projectID'];
//        }
//
//
//        $this->general_variables = [
//			'cities_list' => $this->cities_list,
//			'citizens_list' => $this->citizens_list,
//            'projects_list' => $this->projects_list
//			];
	}

	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	public function rand_string( $length ) {
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";	

		$size = strlen( $chars );
		for( $i = 0; $i < $length; $i++ ) {
			$str .= $chars[ rand( 0, $size - 1 ) ];
		}

		return $str;
	}
	
	
			


}
