<?php
class Task5Controller extends BaseController {

    public function __construct()
    {
        parent::__construct();

    }

    public function task_5(){
        // dd($this->general_variables);


        $number_of_cities = DB::table('cities')->count();
        $number_of_citizens = DB::table('citizens')->count();
        $number_of_projects = DB::table('projects')->count();

        return View::make('task_5_layout')->with(array(
            'tasks_presentation' => $this->tasks_presentation,
            'number_of_cities' => $number_of_cities,
            'number_of_citizens' => $number_of_citizens,
            'number_of_projects' => $number_of_projects
        ));
    }

    public function search_by_id(){
        $input = Input::All();

        $citizen = Citizen::find($input['search_bar']);

        if (!empty($citizen)) {
            echo json_encode(array(
                'error' => 0,
                'fname' => $citizen->fname,
                'lname' => $citizen->lname,
                'address' => $citizen->address,
                'age' => $citizen->age,
                'email' => $citizen->email,
                'mobile' => $citizen->mobile
            ));
        } else {
            echo json_encode(array('error'=> 1)) ;
        }

    }

}
?>
