<?php 
	class Task2Controller extends BaseController {

        public $cities = [];
        public $cities_list = array();

        public $citizens = [];
        public $citizens_list = array();

        public $projects = [];
        public $projects_list =[];

        public $general_variables = [];

		public function __construct()
			{
				parent::__construct();


//                // citizens list
//            $this->citizens = DB::table('citizens')->lists('id', 'fname', 'lname', 'address');
//            foreach ($this->citizens as $citizen => $citizen_id) {
//                $this->citizens_list[$citizen_id] = $citizen;
//            };
//
//                //projects list
//                $this->projects = DB::table('projects')->lists('id', 'projectID');
//                foreach ($this->projects as $project => $project_id) {
//                    $this->projects_list[$project_id] = $project;
//                };


                $this->general_variables = [
                    'cities_list' => $this->cities_list,
                    'citizens_list' => $this->citizens_list,
                    'projects_list' => $this->projects_list
                    ];
			}

		public function task_2(){						
			// dd($this->general_variables);
            $number_of_cities = DB::table('cities')->count();
            $number_of_citizens = DB::table('citizens')->count();
            $number_of_projects = DB::table('projects')->count();

            $last_city_created = City::orderBy('updated_at', 'DESC')->first();
            $last_citizen_created = Citizen::orderBy('updated_at', 'DESC')->first();
            $last_project_created = Project::orderBy('updated_at', 'DESC')->first();

            return View::make('task_2_layout')->with(array(
                'general_variables' => $this->general_variables,
                'tasks_presentation' => $this->tasks_presentation,
                'number_of_cities' => $number_of_cities,
                'number_of_citizens' => $number_of_citizens,
                'number_of_projects' => $number_of_projects,
                'last_city_created' => $last_city_created,
                'last_citizen_created' => $last_citizen_created,
                'last_project_created' => $last_project_created
            ));
		}

		public function create_city(){

			$input= Input::all();

            $validation = Validator::make(Input::all(), [
                'name' => 'required',
                'state' => 'required|max:2',
                'population' => 'required|numeric',
                'zipcode' => 'required|numeric',
                'area' => 'required|numeric',
                'phone' => 'required|numeric',
                'fax' => 'required|numeric'
            ]);

            if ($validation->fails()){
                return Redirect::back()->withInput()->withErrors($validation->messages());
            }

			$city = new City;
			$city->name = $input['name'];
			$city->state = $input['state'];
			$city->population = $input['population'];
			$city->zipcode = $input['zipcode'];
			$city->area = $input['area'];
			$city->phone = $input['phone'];
			$city->fax = $input['fax'];
			$city->save();

            return Redirect::back()->with($this->general_variables)->with('city_createdd', $city);

		}

		public function create_citizen(){
			$input = Input::all();			
			// create new citizen

            $validation = Validator::make(Input::all(),[
                'city' => 'required',
                'fname' => 'required',
                'lname' => 'required',
                'age' => 'required|integer|max:150',
                'address' => 'required',
                'mobile' => 'required|numeric',
                'email' => 'required|email'
            ]);

            if ($validation->fails()) {
                return Redirect::back()->withInput()->withErrors($validation->messages());
            }

			$citizen = new Citizen;
			
			$citizen->fname = $input['fname'];
			$citizen->lname = $input['lname'];
			$citizen->age = $input['age'];
			$citizen->address = $input['address'];
			$citizen->mobile = $input['mobile'];
            $citizen->email = $input['email'];
			$citizen->save();

            $city = City::find($input['city']);
            $city->population = $city->population + 1;
            $city->save();


			// create new enrollment
			$city_selected = DB::table('cities')->where('name', $input['city'])->get();
			$city_id = $input['city'];

			$enrollment = new Enrollmentone;
			$enrollment->city_id = $city_id;
			$enrollment->citizen_id = $citizen->id;
			$enrollment->save();

            return Redirect::back()->with($this->general_variables)->with('citizen_created', $citizen);
		}

		public function create_project(){
			$input = Input::all();

            $validation = Validator::make(Input::all(),[
                'citizenID' => 'required|numeric',
                'projectID' => 'required|numeric',
                'description' => 'required|max:100',
                'date_start' => 'required|date_format:Y-m-d',
                'date_end' => 'required|date_format:Y-m-d',
                'amount' => 'required|numeric'
            ]);

            if ($validation->fails()) {
                return Redirect::back()->withInput()->withErrors($validation->messages());
            }

            $project = new Project;
            $project->projectID = $input['projectID'];
            $project->description = $input['description'];
            $project->date_start = $input['date_start'];
            $project->date_end = $input['date_end'];
            $project->amount = $input['amount'];
            $project->save();

            $enrollment = new Enrollmenttwo;
            $enrollment->citizen_id = $input['citizenID'];
            $enrollment->project_id = $project->id;
            $enrollment->save();

            return Redirect::back()->with($this->general_variables)->with('project_created', $project);
		}

        public function show_city_details(){
//            This $input is the id of the selected city
            $input = Input::all();

            $number_of_city_citizens = DB::table('enrollmentsone')->where('city_id', '=', $input['select_city'])->count();

            $number_of_city_projects = DB::table('enrollmentstwo')
                ->leftJoin('enrollmentsone', 'enrollmentstwo.citizen_id', '=', 'enrollmentsone.citizen_id')
                ->where('enrollmentsone.city_id', '=', $input['select_city'])
                ->count();

            $city = City::find($input['select_city']);
            $area = $city->area;
            $name_of_city = $city->name;

            $population_x_area = round($number_of_city_citizens / $area);

            echo json_encode (array(
                'name_of_city' => $name_of_city,
                'number_of_citizens' => $number_of_city_citizens,
                'number_of_projects' => $number_of_city_projects,
                'population_x_area' => $population_x_area
            ));
        }

        public function delete_city(){
            $input = Input::all();
            $city = City::find($input['select_city']);
            $city->delete();

            return Redirect::back()->with($this->general_variables)->with('city_deleted', $city);


        }

        public function delete_citizen(){
            $input = Input::all();

            $citizen = Citizen::find($input['select_citizen']);
            $citizen_id = $input['select_citizen'];
            $citizen->delete();

            DB::table('enrollmentsone')->where('citizen_id', $citizen_id)->delete();
//            $enrollment = Enrollmentone::where('citizen_id', '=', $citizen_id)->take(1)->get();


            return Redirect::back()->with($this->general_variables)->with('citizen_deleted', $citizen);
        }

        public function delete_project(){
            $input = Input::all();
            $project_id = $input['select_project'];
            $project = Project::find($input['select_project']);
            $project->delete();

            DB::table('enrollmentstwo')->where('project_id', $project_id)->delete();

            return Redirect::back()->with($this->general_variables)->with('project_deleted', $project);
        }

        public function edit_city(){
            $input = Input::all();
            $city = City::find($input['select_city_edit']);

            $city_id = $input['select_city_edit'];

            echo json_encode(array(
                'city_id' => $city->id,
                'city_name' => $city->name,
                'city_state' => $city->state,
                'city_population' => $city->population,
                'city_zipcode' => $city->zipcode,
                'city_area' => $city->area,
                'city_phone' => $city->phone,
                'city_fax' => $city->fax
            ));

//            return View::make('edit.edit_city')->with('city' , $city)->with('city_id', $city_id);
        }

        public function edit_selected_city(){
            $input = Input::all();

            $city = City::find($input['city_id']);

            $city->name = $input['name'];
            $city->state = $input['state'];
            $city->population = $input['population'];
            $city->zipcode = $input['zipcode'];
            $city->area = $input['area'];
            $city->phone = $input['phone'];
            $city->fax = $input['fax'];

            $city->save();

//            return Redirect::to('task_2')->with($this->general_variables);
        }

        public function edit_citizen(){
            $input = Input::all();

            $citizen_id = $input['select_citizen'];
            $citizen = Citizen::find($citizen_id);

            echo json_encode(array(
                'citizen_id' => $citizen->id,
                'citizen_fname' => $citizen->fname,
                'citizen_lname' => $citizen->lname,
                'citizen_age' => $citizen->age,
                'citizen_address' => $citizen->address,
                'citizen_mobile' => $citizen->mobile,
                'citizen_email' => $citizen->email,
            ));

//            return View::make('edit.edit_citizen')->with('citizen_id', $citizen_id)->with('citizen', $citizen);


        }

        public function edit_selected_citizens(){
            $input = Input::all();

            $citizen_id = $input['citizen_id'];
            $citizen = Citizen::find($citizen_id);

            $citizen->fname = $input['fname'];
            $citizen->lname = $input['lname'];
            $citizen->age = $input['age'];
            $citizen->address = $input['address'];
            $citizen->mobile = $input['mobile'];
            $citizen->email = $input['email'];

            $citizen->save();

            return Redirect::to('task_2')->with($this->general_variables);
        }

        public function edit_project(){
            $input = Input::all();
            $project_id = $input['select_project_edit'];
            $project = Project::find($project_id);

            echo json_encode(array(
                'project_id' => $project->id,
                'projectID' => $project->projectID,
                'description' => $project->description,
                'date_start' => $project->date_start,
                'date_end' => $project->date_end
            ));

//            return View::make('edit.edit_project')->with('project_id', $project_id)->with('project', $project);
        }

        public function edit_selected_project(){
            $input = Input::all();

            $project_id = $input['project_id'];

            $project = Project::find($project_id);

            $project->projectID = $input['projectID'];
            $project->description = $input['description'];
            $project->date_start = $input['date_start'];
            $project->date_end = $input['date_end'];
            $project->amount = $input['amount'];

            $project->save();


            return Redirect::to('task_2')->with($this->general_variables);
        }

        public function show_cities(){
            // cities list
            $this->cities = DB::table('cities')->get();

            foreach ($this->cities as $city) {
                $this->cities_list[$city->id] = $city->name;
            }

            if(!empty($this->cities_list)) {
                echo json_encode(array('error' => 0, 'result' => $this->cities_list));
            } else {
                echo json_encode(array('error' => 1, 'error_message' => 'Nu exista inregistrari'));
            }
        }

        public function show_citizens(){
            // citizens list
            $this->citizens = DB::table('citizens')->lists('id', 'lname');


            foreach ($this->citizens as $citizen => $citizen_id) {
                $this->citizens_list[$citizen_id] = $citizen;
            };

            if(!empty($this->citizens_list)) {
                echo json_encode(array('error' => 0, 'result' => $this->citizens_list));
            } else {
                echo json_encode(array('error' => 1, 'error_message' => 'Nu exista inregistrari'));
            }
        }
    }
?>
