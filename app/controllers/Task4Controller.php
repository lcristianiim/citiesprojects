<?php
class Task4Controller extends BaseController {

    public function __construct()
    {
        parent::__construct();

    }

    public function task_4(){
        // dd($this->general_variables);


        $number_of_cities = DB::table('cities')->count();
        $number_of_citizens = DB::table('citizens')->count();
        $number_of_projects = DB::table('projects')->count();

        return View::make('task_4_layout')->with(array(
            'tasks_presentation' => $this->tasks_presentation,
            'number_of_cities' => $number_of_cities,
            'number_of_citizens' => $number_of_citizens,
            'number_of_projects' => $number_of_projects
        ));
    }

    public function send_emails(){

        $number_of_cities = DB::table('cities')->count();
        $number_of_citizens = DB::table('citizens')->count();
        $number_of_projects = DB::table('projects')->count();

        $now = new DateTime;

        $citizens = DB::table('enrollmentstwo')
            ->leftJoin('projects', 'enrollmentstwo.project_id', '=', 'projects.id')
            ->where(function($query) use ($now) {
                $query->where('date_end', '<', $now);
            })
            ->get();

//
//        $test_query = DB::table('database_table')
//            ->where(function($query) use($q) {
//                $query->where('field1', 'like', '%'.$q.'%');
//                $query->or_where('field2', 'like', '%'.$q.'%');
//                $query->or_where('field3', 'like', '%'.$q.'%');
//            })
//            ->where('fieldx', '=', 'active')
//            ->order_by('fieldy', 'asc');


        if (!empty($citizens)) {

            $list_of_citizens_to_be_sent_email = [];

            foreach ($citizens as $citizen) {
                if ($citizen->date_end < $now) {
                    $list_of_citizens_to_be_sent_email[] = $citizen->citizen_id;
                }
            }

            $email_to_id = array_unique($list_of_citizens_to_be_sent_email);


            $citizens_exact = DB::table('citizens')
                ->whereIn('id', $email_to_id)->get();

            $list_emails = [];
            foreach ($citizens_exact as $item) {
                $list_emails[] = $item->email;
            }

            $data = [];

            $var1 = array_unique($list_emails);
            $number_of_emails = count($var1);

            foreach ($var1 as $email) {
                Mail::send('emails.default_mail', $data, function($message) use ($email){
                    $message->to($email)
                        ->subject('Notificare Citiesprojects');
                });
            }

            echo json_encode(array(
                'number_of_emails' => $number_of_emails
            ));
        } else {
            echo json_encode(array(
                'number_of_emails' => 0
            ));
        }

    }

}
?>
