<?php

class Task1Controller extends BaseController {

	/*
	,--------------------------------------------------------------------------
	, Default Home Controller
	,--------------------------------------------------------------------------
	,
	, You may wish to use controllers instead of, or in addition to, Closure
	, based routes. That's great! Here is an example controller method to
	, get you started. To route to this controller, just add the route:
	,
	,	Route::get('/', 'HomeController@showWelcome');
	,
	*/


	/*
	 *la fiecare controller ce extinde base controlerulda
	 */
	public function __construct()
	{
		parent::__construct(); 
	}

    public function task_1(){

        $number_of_cities = DB::table('cities')->count();
        $number_of_citizens = DB::table('citizens')->count();
        $number_of_projects = DB::table('projects')->count();

        return View::make('task_1_layout')->with(array(
            'tasks_presentation' => $this->tasks_presentation,
            'number_of_cities' => $number_of_cities,
            'number_of_citizens' => $number_of_citizens,
            'number_of_projects' => $number_of_projects
        ));
    }

    public function task_1_kill(){
		error_reporting(E_ALL ^ E_DEPRECATED);
        $hostname = 'localhost';
        $username = 'root';
        $password = 'xsvh12wtwt';
        mysql_connect($hostname, $username, $password);

        $result = mysql_query("SHOW FULL PROCESSLIST");

        while ($row=mysql_fetch_array($result)) {
            $process_id=$row["Id"];
            if ($row["User"] = 'root' ) {
                $sql="KILL $process_id";
                mysql_query($sql);
            }
        }



        return Redirect::to('task_1');
    }

    public function task_1_execute(){
        set_time_limit ( 0 );
        ini_set('memory_limit', '5G');
        ini_set('display_errors','On');
        ini_set('error_reporting',E_ALL);
        ini_set('max_execution_time', 10000);

        //Deleting all data from database
        DB::statement('TRUNCATE TABLE cities');
        DB::statement('TRUNCATE TABLE citizens');
        DB::statement('TRUNCATE TABLE enrollmentsone');
        DB::statement('TRUNCATE TABLE enrollmentstwo');
        DB::statement('TRUNCATE TABLE projects');


        // CITIES
	    // set the number of cities
		$minimum_number_of_cities = 10;
		$maximum_number_of_cities = 11;

		$number_of_cities = rand($minimum_number_of_cities, $maximum_number_of_cities);
		$cities = array();

	    //define the random string function used to generate random citie names, etc..
		function rand_string( $length, $abreviation ) {
			$str = $abreviation;
			$chars = "abcdefghijklmnopqrstuvwxyz";	

			$size = strlen( $chars );
			for( $i = 0; $i < $length; $i++ ) {
				$str .= $chars[ rand( 0, $size - 1 ) ];
			}
			return $str;
		}

	    //define the function that returns random number based on lenght an abreviation. ex zipcode
		function rand_zip( $length ) {
			$str = null;
			$chars = "123456789";	

			$size = strlen( $chars );
			for( $i = 0; $i < $length; $i++ ) {
				$str .= $chars[ rand( 0, $size - 1 ) ];
			}

			return $str;
		}
	    // define the function that sets a random state
		function rand_state(){
			$aStates = array(
			'AB', 'AR', 'AG', 'BC' , 'BH' , 'BN' , 'BT' , 'BV' , 'BR', 'BZ' , 'CS' , 'CL' , 'CJ' , 'CT' , 'CV' , 'DB' , 'DJ' , 'GL' , 'GR' , 'GJ' , 'HR' , 'HD' , 'IL' , 'IS' , 'IF' , 'MM' , 'MH' , 'MS' , 'NT' , 'OT' , 'PH' , 'SM' , 'SJ' , 'SB' , 'SV' , 'TR' , 'TM' , 'TL' , 'VS' , 'VL' , 'VN' 
			);
		return $aStates[rand(0, count($aStates) - 1)];
		}
	    // define the function that sets a random number for of population, etc..
		function rand_number($min,$max){
			$rand_number = rand($min, $max);
			return $rand_number;
		}
	    // define the function that sets a random date
		function randomDate($start_date, $end_date)
		{
		    // Convert to timetamps
		    $min = strtotime($start_date);
		    $max = strtotime($end_date);

		    // Generate random number using above bounds
		    $val = rand($min, $max);

		    // Convert back to desired date format
		    return date('Y-m-d H:i:s', $val);
		}

	    // creating organised random data in database
		for ($i=0; $i < 10; $i++) {
            if ($i == 9) { break; }

			// creating the city
			$cities[$i] = new City;
			$cities[$i]->name = rand_string(6, 'City_');
			$cities[$i]->state = rand_state();
			$cities[$i]->population = rand_number(10000,11000);
			$cities[$i]->zipcode = rand_zip(6);
			$cities[$i]->area = rand_number(30,500);
			$cities[$i]->phone = rand_zip(10);
			$cities[$i]->fax = rand_zip(10);
			$cities[$i]->save();
            $city_idd = $cities[$i]->id;

			// creating the population for the current city
			for ($j=0; $j < $cities[$i]->population; $j++) { 
				$citizen = new Citizen;

				$citizen->fname = rand_string(6, 'fname_');
				$citizen->lname = rand_string(6, 'lname_');
				$citizen->age = rand_number(18,85);
				$address = rand_string(8, 'str.');
				$address .= rand_number(1,250);
				$citizen->address = $address;
				$citizen->mobile = rand_zip(10);
				$citizen->save();
                $citizen_idd = $citizen->id;
				
				// set the enrollment for current citizen with match rule city_id = citizen_id
				$enrollment = new Enrollmentone;
				$enrollment->city_id = $city_idd;
				$enrollment->citizen_id = $citizen_idd;
				$enrollment->save();

				for ($k=1; $k < 4; $k++) { 
					$project = new Project;
					$project->projectID = rand_zip(7);
					$project->description = rand_string(100, 'description_');
					$project->date_start = randomDate('1 January 2014', '28 december 2014');
					$project->date_end = randomDate('1 January 2015', '28 december 2015');
					$project->amount = rand_number(1000, 50000);
					$project->save();
                    $project_idd = $project->id;

					// set the enrollment for current project with match rule project_id = citizen_id
					$enrollment = new Enrollmenttwo;
					$enrollment->project_id = $project_idd;
					$enrollment->citizen_id = $citizen_idd;
					$enrollment->save();

				}
			}		
		}
		$cities = City::all()->toArray();
			$cities_list = array();
			
			foreach ($cities as $city) {
				$cities_list[] = $city['name'];
			}			
	return Redirect::to('task_1');
	}

}
