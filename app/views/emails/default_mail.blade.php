<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<div>

    <div class="bs-callout bs-callout-danger">
        <h4>Notificare expirare proiect</h4>
        <p>Atentie unul sau mai multe proiecte inregistrate la <a href="http://162.243.86.132:10006/">citiesprojects</a> a expirat.
            Te rugam sa iei legatura cu <strong>administratia citiesprojects</strong> pentru mai multe detalii.</p>
    </div>
    <footer>
        --</br>
        KIND REGARDS,</br>
        CitiesProjects by Cristian-Marcel Szabo</br>
        Mobile: 0040 724 051 201</br>
        ....................................................................................</br>
        Privileged/Confidential Information may be contained in this message. If</br>
        you are not in the addresses indicated in this message (or responsible</br>
        for delivery of the message to such person), you may not copy or deliver</br>
        this message to anyone. In such a case, you should destroy this message</br>
        and kindly notify the sender by reply e-mail.</br>

    </footer>
</div>
</body>
</html>
