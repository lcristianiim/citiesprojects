<html>

    <head>
        <title>Edit projects</title>
        <script src="<?= asset('bootstrap-master') ?>/dist/js/bootstrap.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="<?= asset('bootstrap-master') ?>/dist/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="">
    </head>

    <body>

        <h2>Edit project</h2>
        <hr>

        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <form accept-charset="UTF-8" action="edit_selected_project" method="POST"><input type="hidden" value="MqxQvbgbgtLMpF0FLUUYSWBRVuCywHHRD5D8feaz" name="_token">

                        <label for="project_id" style="visibility: hidden">Project_ID</label>
                        <input type="text" name="project_id" style="visibility: hidden" value="{{$project_id}}"/>
                        <br>

                        <label for="projectID">ID Proiect</label>
                        <input type="text" name="projectID" value="{{$project->projectID}}" class="form-control">

                        <label for="description">Descriere</label>
                        <input type="text" name="description" value="{{$project->description}}" class="form-control">

                        <label for="date_start">Data inceputului proiectului</label>
                        <input type="text" name="date_start" value="{{$project->date_start}}" class="form-control">

                        <label for="date_end">Data sfarsitului proiectului</label>
                        <input type="text" name="date_end" value="{{$project->date_end}}" class="form-control">

                        <label for="amount">Suma</label>
                        <input type="text" name="amount" value="{{$project->amount}}" class="form-control">

                        <input type="submit" value="Editare oras" class="btn btn-default">
                </div>
                </form>
            </div>
        </div>


    </body>

</html>
