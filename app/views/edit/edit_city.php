<html>

    <head>
        <title>Edit cities</title>
        <script src="<?= asset('bootstrap-master') ?>/dist/js/bootstrap.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="<?= asset('bootstrap-master') ?>/dist/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="">
    </head>
    
    <body>
    
        <h2>Edit city</h2>
        <hr>
        
        <div>
            <form action="edit_selected_city" method="post">
                <label for="name">Numele orasului</label>
                <input type="text"/>
            </form>
        </div>

        <div>
            <form accept-charset="UTF-8" action="http://162.243.86.132:10006/create_city" method="POST"><input type="hidden" value="MqxQvbgbgtLMpF0FLUUYSWBRVuCywHHRD5D8feaz" name="_token">
                <input type="text" name="name" placeholder="Numele orasului" class="form-control">
                <input type="text" name="state" placeholder="Statul" class="form-control">
                <input type="text" name="population" placeholder="Numarul de locuitori" class="form-control">
                <input type="text" name="zipcode" placeholder="Codul postal" class="form-control">
                <input type="text" name="area" placeholder="Suprafata km2" class="form-control">
                <input type="text" name="phone" placeholder="Telefon" class="form-control">
                <input type="text" name="fax" placeholder="fax" class="form-control">
                <input type="submit" value="Create city" class="btn btn-default">
            </form>
        </div>
        
    
    </body>

</html>
