<html>

    <head>
        <title>Edit cities</title>
        <script src="<?= asset('bootstrap-master') ?>/dist/js/bootstrap.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="<?= asset('bootstrap-master') ?>/dist/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="">
    </head>
    
    <body>
    
        <h2>Edit city</h2>
        <hr>

        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <form accept-charset="UTF-8" action="edit_selected_city" method="POST"><input type="hidden" value="MqxQvbgbgtLMpF0FLUUYSWBRVuCywHHRD5D8feaz" name="_token">

                        <label for="city_id" style="visibility: hidden">City_ID</label>
                        <input type="text" name="city_id" style="visibility: hidden" value="{{$city_id}}"/>
                        <br/>

                        <label for="name">Numele orasului</label>
                        <input type="text" name="name" value="{{$city->name}}" class="form-control">

                        <label for="state">Statul sau judetul</label>
                        <input type="text" name="state" value="{{$city->state}}" class="form-control">

                        <label for="population">Populatie</label>
                        <input type="text" name="population" value="{{$city->population}}" class="form-control">

                        <label for="zipcode">Cod postal</label>
                        <input type="text" name="zipcode" value="{{$city->zipcode}}" class="form-control">

                        <label for="area">Suprafata</label>
                        <input type="text" name="area" value="{{$city->area}}" class="form-control">

                        <label for="phone">Telefon</label>
                        <input type="text" name="phone" value="{{$city->phone}}" class="form-control">

                        <label for="fax">Fax</label>
                        <input type="text" name="fax" value="{{$city->fax}}" class="form-control">

                        <input type="submit" value="Editare oras" class="btn btn-default">
                </div>
                </form>
            </div>
        </div>
        
    
    </body>

</html>
