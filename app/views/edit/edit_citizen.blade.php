<html>

    <head>
        <title>Edit citizens</title>
        <script src="<?= asset('bootstrap-master') ?>/dist/js/bootstrap.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="<?= asset('bootstrap-master') ?>/dist/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="">
    </head>
    
    <body>
    
        <h2>Edit citizen</h2>
        <hr>

        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <form accept-charset="UTF-8" action="edit_selected_citizens" method="POST"><input type="hidden" value="MqxQvbgbgtLMpF0FLUUYSWBRVuCywHHRD5D8feaz" name="_token">

                        <label for="citizen_id" style="visibility: hidden">City_ID</label>
                        <input type="text" name="citizen_id" style="visibility: hidden" value="{{$citizen_id}}"/>
                        <br/>

                        <label for="fname">Prenumele cetateanului</label>
                        <input type="text" name="fname" value="{{$citizen->fname}}" class="form-control">

                        <label for="lname">Numele cetateanului</label>
                        <input type="text" name="lname" value="{{$citizen->lname}}" class="form-control">

                        <label for="age">Varsta</label>
                        <input type="text" name="age" value="{{$citizen->age}}" class="form-control">

                        <label for="address">Adresa</label>
                        <input type="text" name="address" value="{{$citizen->address}}" class="form-control">

                        <label for="mobile">Telefon mobil</label>
                        <input type="text" name="mobile" value="{{$citizen->mobile}}" class="form-control">

                        <input type="submit" value="Editare cetatean" class="btn btn-default">
                </div>
                </form>
            </div>
        </div>
        
    
    </body>

</html>
