@extends('master_page')

@section('content')
<p class="pull-right visible-xs">
    <button data-toggle="offcanvas" class="btn btn-primary btn-xs" type="button">Toggle nav</button>
</p>
<div class="jumbotron">
    <h1>Cities projects</h1>
    <p>This is an evaluation test with 5 tasks. The main accent is on server-side programing. </p>
</div>

<div class="row">
    <div class="col-12 col-sm-12 col-lg-12">
        <h4>Problema</h4>
        <p>Se considera o baza de date ce contine urmatoarele tabele: orase, locuitori, proiecte. Fiecare oras poate avea mai multi locuitori (relatia fiind de 1:n). De asemenea un locuitor poate lucra la mai multe proiecte in acelasi timp (relatia fiind de n:m).</p>

        <p>Cele 3 tabele au urmatoarele campuri obligatorii: (Daca, pentru realizarea proiectului ai nevoie si de alte campuri, acestea pot fi adaugate. Dar cele specificate mai jos sunt obligatorii. La fel, daca ai nevoie de tabele aditionale, acestea pot fi adaugate).</p>

        <p>city: name (varchar), state (char), population (int), zipcode (varchar), area (int) – km patrati, phone (varchar), fax (varchar).</p>
        <p>citizen: fname (varchar), lname (varchar), age (int), address (varchar), mobile (varchar).</p>
        <p>project: projID (varchar), description (text), date_start (date), date_end (date), amount (float).</p>
    </div>
</div>

<div class="row">
    @foreach ($tasks_presentation as $task)
        <div class="col-6 col-sm-6 col-lg-4">
            <h2>{{$task['heading']}}</h2>
            <p>{{$task['paragraph']}}</p>
            <p><a role="button" href="{{ $task['link'] }}" class="btn btn-default">{{$task['heading']}} »</a></p>
        </div><!--/span-->
    @endforeach
</div>
@stop



