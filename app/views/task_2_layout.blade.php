@extends('master_page')

@section('content')

<div class="row">

    <script type="text/javascript">


        //manipulate the cities list with function manipulate_db_data from task_2_scripts.js
        var var0 = ".city_supp";
        var var1 = ".selectcity";
        var var2 = ".cities_list";
        var post = "show_cities";
        manipulate_db_data(var0, var1, var2, post);

        //manipulate the citizens list with function manipulate_db_data from task_2_scripts.js
        var var0 = ".citizens_supp";
        var var1 = ".selectcitizen";
        var var2 = ".citizens_list";
        var post = "show_citizens";
        manipulate_db_data(var0, var1, var2, post);



        form_city_details();

        search_for_citizen('#form_search_citizen', '#show_result_2', '#show_loading', '#div_delete_citizen', '#show_result');
        search_for_citizen('#form_search_citizen_second', '#show_result_2_second', '#show_loading_second', '#div_delete_citizen_second', '#show_result_second');
        modal_1();
        modal_2();
        modal_3();




    </script>


    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">{{$tasks_presentation[1]['heading']}}</div>
        <div class="panel-body">
            <p>{{$tasks_presentation[1]['paragraph']}}</p>
        </div>

        <!-- List group -->
        <ul class="list-group">
            <li class="list-group-item well">
                <div class="row ">
                    <h4 class="text-center">Adaugare date</h4>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <!-- Adaugare orase -->
                        <div class="row-fluid">

                            {{Form::open(array('url' => 'create_city', 'id' => 'form_create_city', 'class' => 'cmxform', 'novalidate' => 'novalidate'))}}
                                <div class="form-group">
                                    <label for="name">Adauga orase:</label>
                                </div>
                                <div class="form-group">
                                    {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Numele orasului')) }}
                                    {{$errors->first('name', '<span class=error>:message</span>' ) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::text('state', null, array('class' => 'form-control', 'placeholder' => 'Judetul')) }}
                                    {{$errors->first('state', '<span class=error>:message</span>' ) }}
                                </div>

                                <div class="form-group">
                                    {{ Form::text('population', null, array('class' => 'form-control', 'placeholder' => 'Numarul de locuitori')) }}
                                    {{$errors->first('population', '<span class=error>:message</span>' ) }}
                                </div>

                                <div class="form-group">
                                    {{ Form::text('zipcode', null, array('class' => 'form-control', 'placeholder' => 'Codul postal')) }}
                                    {{$errors->first('zipcode', '<span class=error>:message</span>' ) }}
                                </div>


                                <div class="form-group">
                                    {{ Form::text('area', null, array('class' => 'form-control', 'placeholder' => 'Suprafata km2')) }}
                                    {{$errors->first('area', '<span class=error>:message</span>' ) }}
                                </div>

                                <div class="form-group">
                                    {{ Form::text('phone', null, array('class' => 'form-control', 'placeholder' => 'Telefon')) }}
                                    {{$errors->first('phone', '<span class=error>:message</span>' ) }}
                                </div>

                                <div class="form-group">
                                    {{ Form::text('fax', null, array('class' => 'form-control', 'placeholder' => 'fax')) }}
                                    {{$errors->first('fax', '<span class=error>:message</span>' ) }}
                                </div>

                                <div class="form-group">
                                    {{ Form::submit('Create city', array('class' => 'btn btn-default')) }}
                                </div>

                            {{Form::close()}}
                        </div>

                        <?php $city_created = Session::get('city_createdd'); ?>
                        @if (isset($city_created) AND !empty($city_created) )
                        <div class="row-fluid">
                            <div class="alert alert-success">
                                Orasul <strong> {{$city_created->name}} </strong> a fost creat! Database id: <strong>{{$city_created->id}}</strong>
                            </div>
                        </div>
                        @endif

                    </div>

                    <div class="col-sm-4">
                        <div class="row-fluid">
                            {{Form::open(array('url' => 'create_citizen'))}}
                            <div class="form-group">
                                <label for="city">Adauga un nou locuitor:</label>
                            </div>
                            <div class="form-group">
                                <input type="button" value="Alege orasul" class="cities_list btn btn-default"/>
                                <select name="city" class="selectcity form-control hidden">
                                </select> </br>
                                {{ $errors->first('city', '<span class=error>:message</span>' ) }}
                            </div>
                            <div class="form-group">
                                {{ Form::text('fname', null, array('class' => 'form-control', 'placeholder' => 'Prenumele')) }}
                                {{ $errors->first('fname', '<span class=error>:message</span>' ) }}
                            </div>
                            <div class="form-group">
                                {{ Form::text('lname', null, array('class' => 'form-control', 'placeholder' => 'Numele')) }}
                                {{ $errors->first('lname', '<span class=error>:message</span>' ) }}
                            </div>
                            <div class="form-group">
                                {{ Form::text('age', null, array('class' => 'form-control', 'placeholder' => 'Varsta')) }}
                                {{ $errors->first('age', '<span class=error>:message</span>' ) }}
                            </div>
                            <div class="form-group">
                                {{ Form::text('address', null, array('class' => 'form-control', 'placeholder' => 'Adresa')) }}
                                {{ $errors->first('address', '<span class=error>:message</span>' ) }}
                            </div>
                            <div class="form-group">
                                {{ Form::text('mobile', null, array('class' => 'form-control', 'placeholder' => 'Telefon mobil')) }}
                                {{ $errors->first('mobile', '<span class=error>:message</span>' ) }}
                            </div>
                            <div class="form-group">
                                {{ Form::text('email', null, array('class' => 'form-control', 'placeholder' => 'Email')) }}
                                {{ $errors->first('email', '<span class=error>:message</span>' ) }}
                            </div>
                            <div class="form-group">
                                {{ Form::submit('Create citizen', array('class' => 'btn btn-default')) }}
                            </div>
                            {{Form::close()}}
                        </div>

                        <?php $citizen_created = Session::get('citizen_created'); ?>
                        @if (isset($citizen_created) AND !empty($citizen_created) )
                        <div class="row-fluid">
                            <div class="alert alert-success">
                                Cetateanul <strong>{{$citizen_created->fname}} {{$citizen_created->lname}}</strong> a fost creat! Database id: <strong>{{$citizen_created->id}}</strong>
                            </div>
                        </div>
                        @endif
                    </div>

                    <div class="col-sm-4">
                        <div class="row-fluid">
                            {{Form::open(array('url' => 'create_project'))}}

                            <div class="form-group">
                                <label for="text">Adauga un nou proiect:</label>
                            </div>
                            <div class="form-group">
                                {{ Form::text('citizenID', null, array('class' => 'form-control', 'placeholder' => 'ID-ul cetateanului din DB mysql')) }}
                                {{ $errors->first('citizenID', '<span class=error>:message</span>' ) }}
                            </div>
                            <div class="form-group">
                                {{ Form::text('projectID', null, array('class' => 'form-control', 'placeholder' => 'ID Proiect')) }}
                                {{ $errors->first('projectID', '<span class=error>:message</span>' ) }}
                            </div>

                            <div class="form-group">
                                {{ Form::text('description', null, array('class' => 'form-control', 'placeholder' => 'Descriere')) }}
                                {{ $errors->first('description', '<span class=error>:message</span>' ) }}
                            </div>

                            <div class="form-group">
                                {{ Form::text('date_start', null, array('class' => 'form-control', 'placeholder' => 'Data inceprii proiectului yyyy-mm-dd')) }}
                                {{ $errors->first('date_start', '<span class=error>:message</span>' ) }}
                            </div>

                            <div class="form-group">
                                {{ Form::text('date_end', null, array('class' => 'form-control', 'placeholder' => 'Data limita a proiectului yyyy-mm-dd')) }}
                                {{ $errors->first('date_end', '<span class=error>:message</span>' ) }}
                            </div>

                            <div class="form-group">
                                {{ Form::text('amount', null, array('class' => 'form-control', 'placeholder' => 'Suma')) }}
                                {{ $errors->first('amount', '<span class=error>:message</span>' ) }}
                            </div>

                            <div class="form-group">
                                {{ Form::submit('Create project', array('class' => 'btn btn-default')) }}
                            </div>

                            {{Form::close()}}

                        </div>

                        <?php $project_created = Session::get('project_created'); ?>
                        @if (isset($project_created) AND !empty($project_created) )
                        <div class="row-fluid">
                            <div class="alert alert-success">
                                Proiectul <strong>{{$project_created->projectID}}</strong> a fost creat! Database id: <strong>{{$project_created->id}}</strong>
                            </div>
                        </div>
                        @endif

                    </div>

                </div>

            </li>
            <!--SHOW DATA-->
            <li class="list-group-item well">
                <!--Shows the city details-->
                <h4 class="text-center">Afisare date pentru oras</h4>
                <div class="row">
                <div class="col-sm-4">
                <form id="form_city_details" action="show_city_details" method="post">
                    <div class="form-group">
                        <label for="butt_list">Orasul:</label>
                    </div>
                    <div class="form-group">
                        <input type="button" value="Afiseaza lista oraselor" class="cities_list btn btn-default" name="butt_list"/>
                    </div>
                    <div class="form-group">
                    <select class="selectcity form-control hidden" name="select_city" id="city_selector" >
                        <?php

                        if (isset($cities_list)) {dd($cities_list);
                            foreach ($cities_list as $key => $value) {
                                echo "<option value=\"" . $key . "\">" . $value . "</option>";
                            }
                        }
                        ?>
                    </select>
                    </div>
                    <div class="form-group">
                    <input id="city_supp_butt" type="submit" value="Afisaza datele orasului" class="city_supp btn btn-default hidden"/>
                    </div>
                </form>

                </div>
                    <div id="to_show_city_data" class="col-sm-4 hidden">
                        <div class="panel panel-default">
                            <div class="panel-body" id="city_data">

                            </div>
                        </div>

                    </div>
                </div>
            </li>
            <!--DELETING ITEMS-->
            <li class="list-group-item well">
                <h4 class="text-center">Stergere date</h4>
                <div class="row">
                    <div class="col-sm-4">
                        <!--delete city-->
                        <form action="delete_city" method="post">
                            <div class="form-group">
                                <label for="select_city">Stergere orase:</label>
                            </div>
                            <div class="form-group">
                                <input type="button" value="Afisaza lista oraselor" class="cities_list btn btn-default"/>
                            </div>

                            <div class="form-group">
                                <select class="selectcity form-control city_selector hidden " name="select_city" id="" >
                                    <?php
                                    if (isset($cities_list)) {
                                        foreach ($cities_list as $key => $value) {
                                            echo "<option value=\"" . $key . "\">" . $value . "</option>";
                                        }
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <input type="submit" value="Sterge orasul" class="city_supp btn btn-default hidden"/>
                            </div>
                        </form>
                        <?php $city_deleted = Session::get('city_deleted'); ?>
                        @if (isset($city_deleted) AND !empty($city_deleted) )
                        <div class="row-fluid">
                            <div class="alert alert-success">
                                Orasul <strong>{{$city_deleted->name}}</strong> a fost sters din baza de date!
                            </div>
                        </div>
                        @endif
                    </div> <!--div-col-sm-4-->

                    <div class="col-sm-4">
                        <!--Delete citizen-->
                        <form id="form_search_citizen">
                            <div class="form-group">
                                <label for="select_citizen">Stergere locuitor: </label>
                            </div>

                            <div class="form-group">
                                {{ Form::text('search_bar', null, array('class' => 'form-control', 'placeholder' => 'Nume, prenume sau adresa')) }}
                            </div>

                            <div class="form-group">
                                <input type="submit" value="Cauta" class="btn btn-default"/>
                            </div>
                        </form>

                        <?php $citizen_deleted = Session::get('citizen_deleted'); ?>
                        @if (isset($citizen_deleted) AND !empty($citizen_deleted) )
                        <div class="row-fluid">
                            <div class="alert alert-success">
                                Cetateanul <strong>{{$citizen_deleted->fname}} {{$citizen_deleted->lname}}</strong> a fost sters din baza de date!
                            </div>
                        </div>
                        @endif

                        <div id="show_loading" class="hidden">

                        </div>

                        <div id="show_result_2" class="hidden">

                        </div>



                        <div id="div_delete_citizen" class="hidden">
                            <form action="delete_citizen" method='post'>
                                <div class="form-group">
                                    <label for="">Rezultate:</label>
                                </div>
                                <div id="show_result" class="form-group">

                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-default" value="Sterge"/>
                                </div>
                            </form>

                        </div>

                    </div>

                    <div class="col-sm-4">

                        <!--Delete project-->
                        <form action="delete_project" method="post">
                            <div class="form-group">
                                <label for="select_project">ID proiect: </label>
                            </div>

                            <div class="form-group">
                                {{ Form::text('select_project', null, array('class' => 'form-control', 'placeholder' => 'Database id')) }}
                            </div>

                            <div class="form-group">
                                <input type="submit" value="Sterge proiectul" class="btn btn-default"/>
                            </div>
                        </form>
                        <?php $project_deleted = Session::get('project_deleted'); ?>
                        @if (isset($project_deleted) AND !empty($project_deleted) )
                        <div class="row-fluid">
                            <div class="alert alert-success">
                                Proiectul <strong>{{$project_deleted->projectID}}</strong> cu Database id: <strong>{{$project_deleted->id}} </strong> a fost sters din baza de date!
                            </div>
                        </div>
                        @endif
                    </div>


                </div>
            </li>
            <!--EDITING ITEMS-->
            <li class="list-group-item well">
                <h4 class="text-center">Editare date:</h4>
                <div class="row">
                    <div class="col-sm-4">


                        <!--Editing cities-->
                        <form id="form_edit_city" action="edit_city" method="post" >
                        <div class="form-group">
                            <label for="">Editare orase:</label>
                        </div>
                        <div class="form-group">
                            <input type="button" value="Afisaza lista oraselor" class="cities_list btn btn-default"/>
                        </div>

                        <div class="form-group">
                            <select id="city_selector_edit_city" class="selectcity form-control hidden city_selector" name="select_city_edit"  >
                                <?php
                                if (isset($cities_list)) {
                                    foreach ($cities_list as $key => $value) {
                                        echo "<option value=\"" . $key . "\">" . $value . "</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <input id="input_form_edit_city" value="Editeaza orasul" class="city_supp btn btn-default hidden " data-toggle="modal" data-target="#myModal_1"/>
                        </div>

                        </form>
                        <!-- Modal -->
                        <div class="modal fade" id="myModal_1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Editare oras: </h4>
                                    </div>
                                    <div id="modal_1_result" class="modal-body">

                                    </div>
                                    <div class="modal-footer">
                                        <button id="close_submit_edit_city" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button id="submit_edit_city" type="submit" class="btn btn-primary">Save changes</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                <div class="col-sm-4">
                    <!--Editing citizens-->
                    <form id="form_search_citizen_second">
                        <div class="form-group">
                            <label for="select_citizen">Editare locuitor: </label>
                        </div>

                        <div class="form-group">
                            {{ Form::text('search_bar', null, array('class' => 'form-control', 'placeholder' => 'Nume, prenume sau adresa')) }}
                        </div>

                        <div class="form-group">
                            <input type="submit" value="Cauta" class="btn btn-default"/>
                        </div>
                    </form>

                    <div id="show_loading_second" class="hidden">

                    </div>

                    <div id="show_result_2_second" class="hidden">

                    </div>



                    <div id="div_delete_citizen_second" class="hidden">
                        <form id="form_edit_citizen" action="edit_citizen" method='post'>
                            <div class="form-group">
                                <label for="">Rezultate:</label>
                            </div>
                            <div id="show_result_second" class="form-group">

                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-default" value="Editeaza" data-toggle="modal" data-target="#myModal_1_second"/>
                            </div>
                        </form>
                    </div>

                    <div class="modal fade" id="myModal_1_second" tabindex="-1" role="dialog" aria-labelledby="myModalLabel_second" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel_second">Editare cetatean: </h4>
                                </div>
                                <div id="modal_2_result_second" class="modal-body">

                                </div>
                                <div class="modal-footer">
                                    <button id="close_submit_edit_citizen" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button id="submit_edit_citizen" type="submit" class="btn btn-primary">Save changes</button>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
<!------------------------------>

                    <!--Edit projects-->
                    <div class="col-sm-4">
                        <form id="form_edit_project" >
                            <div class="form-group">
                                <label for="select_project_edit">Editare proiecte:</label>
                            </div>
                            <div class="form-group">

                                <input id="project_edit_selector" class="form-control" type="text" name="select_project_edit" placeholder="Database id" />

                            </div>
                            <div class="form-group">
                                <input type="submit" value="Editeaza proiectul" class="btn btn-default" data-toggle="modal" data-target="#myModal_1_third"/>
                            </div>
                        </form>
                    </div>

                    <div class="modal fade" id="myModal_1_third" tabindex="-1" role="dialog" aria-labelledby="myModalLabel_third" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel_third">Editare proiect: </h4>
                                </div>
                                <div id="modal_1_result_third" class="modal-body">

                                </div>
                                <div class="modal-footer">
                                    <button id="close_submit_edit_project" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button id="submit_edit_project" type="submit" class="btn btn-primary">Save changes</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </li>




            <li class="list-group-item">

            </li>
        </ul>
    </div>

</div>


@stop

@section('right_link')

<div role="navigation" id="sidebar" class="col-xs-6 col-sm-3 sidebar-offcanvas">
    <div class="list-group">
        <div class="panel panel-default">
            <div class="panel-heading">Database records</div>
            <div class="panel-body"> Cities: {{$number_of_cities}} </div>
            <div class="panel-body"> Population: {{$number_of_citizens}} </div>
            <div class="panel-body"> Projects: {{$number_of_projects}} </div>
        </div>
    </div>
</div><!--/span-->

<div role="navigation" id="sidebar" class="col-xs-6 col-sm-3 sidebar-offcanvas">
    <div class="list-group">
        <div class="panel panel-default">
            <div class="panel-heading">Last city recorded or modified</div>
            <div class="panel-body"> Database id: {{$last_city_created->id}}  </div>
            <div class="panel-body"> Name: {{$last_city_created->name}} </div>
            <div class="panel-body"> Judet: {{$last_city_created->state}} </div>
            <div class="panel-body"> Population: {{$last_city_created->population}}  </div>
        </div>
    </div>
</div><!--/span-->

<div role="navigation" id="sidebar" class="col-xs-6 col-sm-3 sidebar-offcanvas">
    <div class="list-group">
        <div class="panel panel-default">
            <div class="panel-heading">Last citizen recorded or modified</div>
            <div class="panel-body"> <strong>Database id:</strong> {{$last_citizen_created->id}}  </div>
            <div class="panel-body"> First Name: {{$last_citizen_created->fname}} </div>
            <div class="panel-body"> Last Name: {{$last_citizen_created->lname}} </div>
            <div class="panel-body"> Age: {{$last_citizen_created->age}} </div>
            <div class="panel-body"> Address: {{$last_citizen_created->address}} </div>
            <div class="panel-body"> Phone: {{$last_citizen_created->mobile}} </div>

        </div>
    </div>
</div><!--/span-->

<div role="navigation" id="sidebar" class="col-xs-6 col-sm-3 sidebar-offcanvas">

    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">Last project recorded or modified</div>
        <div class="panel-body"> <strong>Database id:</strong> {{$last_project_created->id}}  </div>
        <div class="panel-body"> ProjectID: {{$last_project_created->projectID}} </div>
        <div class="panel-body"> Amount: {{$last_project_created->amount}} </div>
        <div class="panel-body"> Date start: {{$last_project_created->date_start}} </div>
        <div class="panel-body"> Date end: {{$last_project_created->date_end}} </div>

    </div>

</div><!--/span-->

@stop