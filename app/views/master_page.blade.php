<html lang="en"><head>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>



    <title>CitiesProjects</title>
    <link rel="stylesheet" type="text/css" href="<?= asset('bootstrap-master') ?>/dist/css/bootstrap.css">

    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= asset('css') ?>/custom.css">
    <link rel="stylesheet" type="text/css" href="<?= asset('css') ?>/offcanvas.css">


    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script src="<?= asset('bootstrap-master') ?>/dist/js/bootstrap.js" type="text/javascript"></script>
    <script src="<?= asset('jquery_validation') ?>/dist/jquery.validate.min.js" type="text/javascript"></script>

    <script src="<?= asset('js')?>/task_2_scripts.js" type="text/javascript"></script>
    <script src="<?= asset('js')?>/custom_scripts.js" type="text/javascript"></script>




    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
    <link href="../../assets/ico/favicon.ico" rel="shortcut icon">

    <script type="text/javascript">
        light_current_links();
    </script>

</head>

<body>
<div role="navigation" class="navbar navbar-fixed-top navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="homepage">CitiesProjects</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li id="home_butt" class=""><a href="homepage">Home</a></li>
            </ul>
        </div><!-- /.nav-collapse -->
    </div><!-- /.container -->
</div><!-- /.navbar -->

<div class="container">

    <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-12 col-sm-9">

            @yield('content')

        </div><!--/span-->


        <div role="navigation" id="sidebar" class="col-xs-6 col-sm-3 sidebar-offcanvas">
            <div class="list-group">
                {{-- */$i=0;/* --}}
                @foreach ($tasks_presentation as $task)
                    {{-- */$i++;/* --}}
                    <a id="task_{{$i}}_butt" class="list-group-item" href="{{ $task['link'] }}">{{$task['heading']}}</a>
                @endforeach
            </div>
        </div><!--/span-->

        @yield('right_link')


    </div><!--/row-->

    <hr>

    <footer>
        <p>&copy; 2014</p>
    </footer>

</div><!--/.container-->



<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="../../dist/js/bootstrap.min.js"></script>
<script src="offcanvas.js"></script>


</body></html>