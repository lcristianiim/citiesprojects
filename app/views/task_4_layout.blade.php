@extends('master_page')


<!--Main content-->
@section('content')

<script>
    send_emails_form();
</script>

<div class="row">
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">{{$tasks_presentation[3]['heading']}}</div>
        <div class="panel-body">
            <p>{{$tasks_presentation[3]['paragraph']}}</p>
        </div>
        <ul class="list-group">
            <div class="row-fluid">
                <li class="list-group-item well">

                    <form id="send_emails_form">
                        <div class="form-group">
                            <label for="button_send_emailss">Trimite e-mail la locuitorii ale caror proiecte au expirat:</label>
                            </div>
                        <div class="form_group">
                            <input id="button_send_emails" name="button_send_emails" type="button" value="Trimite" class="btn btn-default"/>
                        </div>
                    </form>

                </li>

                <li id="status" class="list-group-item hidden">

                </li>



            </div>

            <div class="container">
                <div class="row-fluid">
                    <div id="content" style="float: none; margin: 0 auto;">

                    </div>
                </div>

            </div>

        </ul>
    </div>
</div>

@stop


<!--Right link supplimentars-->
@section('right_link')
<div role="navigation" id="sidebar" class="col-xs-6 col-sm-3 sidebar-offcanvas">
    <div class="list-group">
        <div class="panel panel-default">
            <div class="panel-heading">Database records</div>
            <div class="panel-body"> Cities: {{$number_of_cities}} </div>
            <div class="panel-body"> Population: {{$number_of_citizens}} </div>
            <div class="panel-body"> Projects: {{$number_of_projects}} </div>
        </div>
    </div>
</div><!--/span-->
@stop