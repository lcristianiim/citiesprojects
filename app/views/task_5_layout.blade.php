@extends('master_page')


<!--Main content-->
@section('content')
<script type="text/javascript">
    $(document).ready(function(){
        $("#search_form").submit(function(){

            $.post('search_by_id', $("#search_form").serialize(), function(data){
                var result = JSON.parse(data);

                    if (result.error == 0) {
                        resultHtml  =   "<table id='search-results'>";
                        resultHtml +=       "<tr>";
                        resultHtml +=           "<td>Prenumele:</td><td>" + result.fname + "</td>";
                        resultHtml +=       "</tr>";
                        resultHtml +=           "<td>Numele:</td><td>" + result.lname + "</td>";
                        resultHtml +=       "</tr>";
                        resultHtml +=           "<td>Varsta:</td><td>" + result.age + "</td>";
                        resultHtml +=       "</tr>";
                        resultHtml +=           "<td>Adresa:</td><td>" + result.address + "</td>";
                        resultHtml +=       "</tr>";
                        resultHtml +=           "<td>Telefon:</td><td>" + result.mobile + "</td>";
                        resultHtml +=       "</tr>";
                        resultHtml +=           "<td>Email:</td><td>" + result.email + "</td>";
                        resultHtml +=       "</tr>";
                        resultHtml  +=   "</table>";

                    } else {

                        resultHtml  =   "<table id='search-results'>";
                        resultHtml +=       "<tr>";
                        resultHtml +=           "<td>Nu exista cetatean cu acest id.</td>";
                        resultHtml +=       "</tr>";
                        resultHtml  +=   "</table>";
                    }
                    $('#content').html(resultHtml);
            })
            return false;
        });

    });
</script>

<!--<script>-->
<!--    $(document).ready(function(){-->
<!--        $("#search_form").submit(function(){-->
<!--            $.post('search_by_id', $("#search_form").serialize(), function(data){-->
<!--                if (result.error == 0) {-->
<!--                    resultHtml  =   "<table id='search-results'>";-->
<!--                    resultHtml +=       "<tr>";-->
<!--                    resultHtml +=           "<td>Prenumele:</td><td>" + result.fname + "</td>";-->
<!--                    resultHtml +=       "</tr>";-->
<!--                    resultHtml +=           "<td>Numele:</td><td>" + result.lname + "</td>";-->
<!--                    resultHtml +=       "</tr>";-->
<!--                    resultHtml +=           "<td>Varsta:</td><td>" + result.age + "</td>";-->
<!--                    resultHtml +=       "</tr>";-->
<!--                    resultHtml +=           "<td>Adresa:</td><td>" + result.address + "</td>";-->
<!--                    resultHtml +=       "</tr>";-->
<!--                    resultHtml +=           "<td>Telefon:</td><td>" + result.mobile + "</td>";-->
<!--                    resultHtml +=       "</tr>";-->
<!--                    resultHtml +=           "<td>Email:</td><td>" + result.email + "</td>";-->
<!--                    resultHtml +=       "</tr>";-->
<!--                    resultHtml  +=   "</table>";-->
<!---->
<!--                } else {-->
<!---->
<!--                    resultHtml  =   "<table id='search-results'>";-->
<!--                    resultHtml +=       "<tr>";-->
<!--                    resultHtml +=           "<td>Prenumele:</td><td>" + result.fname + "</td>";-->
<!--                    resultHtml +=       "</tr>";-->
<!--                    resultHtml +=           "<td>Numele:</td><td>" + result.lname + "</td>";-->
<!--                    resultHtml +=       "</tr>";-->
<!--                    resultHtml +=           "<td>Varsta:</td><td>" + result.age + "</td>";-->
<!--                    resultHtml +=       "</tr>";-->
<!--                    resultHtml +=           "<td>Adresa:</td><td>" + result.address + "</td>";-->
<!--                    resultHtml +=       "</tr>";-->
<!--                    resultHtml +=           "<td>Telefon:</td><td>" + result.mobile + "</td>";-->
<!--                    resultHtml +=       "</tr>";-->
<!--                    resultHtml +=           "<td>Email:</td><td>" + result.email + "</td>";-->
<!--                    resultHtml +=       "</tr>";-->
<!--                    resultHtml  +=   "</table>";-->
<!--                }-->
<!--                $('#content').html(resultHtml);-->
<!--            })-->
<!--            return false;-->
<!--        })-->
<!--    })-->
<!--</script>-->

<div class="row">
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">{{$tasks_presentation[4]['heading']}}</div>
        <div class="panel-body">
            <p>{{$tasks_presentation[4]['paragraph']}}</p>
        </div>
        <ul class="list-group">
            <div class="row-fluid">
                <li class="list-group-item well">

                    <form id="search_form">
                        <input type="text" name="search_bar" class="form-control form-group" placeholder="Citizen's database id"/>
                        <input type="submit" value="Cauta" class="btn btn-default"/>
                    </form>

                </li>
            </div>

            <div class="container">
                <div class="row-fluid">
                    <div id="content" style="float: none; margin: 0 auto;">

                    </div>
                </div>

            </div>

        </ul>
    </div>
</div>

@stop


<!--Right link supplimentars-->
@section('right_link')
<div role="navigation" id="sidebar" class="col-xs-6 col-sm-3 sidebar-offcanvas">
    <div class="list-group">
        <div class="panel panel-default">
            <div class="panel-heading">Database records</div>
            <div class="panel-body"> Cities: {{$number_of_cities}} </div>
            <div class="panel-body"> Population: {{$number_of_citizens}} </div>
            <div class="panel-body"> Projects: {{$number_of_projects}} </div>
        </div>
    </div>
</div><!--/span-->
@stop