<html>
<head>
	<title>CitiesProjects tasks</title>
	<script src="<?= asset('bootstrap-master') ?>/dist/js/bootstrap.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="<?= asset('bootstrap-master') ?>/dist/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?= asset('css') ?>/custom.css">
    <script src="<?= asset('js')?>/task_2_scripts.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

</head>
<body>

<script type="text/javascript">

    //manipulate the cities list with function manipulate_db_data from task_2_scripts.js
    var var0 = ".city_supp";
    var var1 = ".selectcity";
    var var2 = ".cities_list";
    var post = "show_cities";
    manipulate_db_data(var0, var1, var2, post);

    //manipulate the citizens list with function manipulate_db_data from task_2_scripts.js
    var var0 = ".citizens_supp";
    var var1 = ".selectcitizen";
    var var2 = ".citizens_list";
    var post = "show_citizens";
    manipulate_db_data(var0, var1, var2, post);

</script>

	<div class="container">
		<h2>Cerinta 2</h2>
		<hr>
		<div>
			2. Sa se implementeze o aplicatie care sa permita urmatoarea functionalitate:
		    adaugarea de noi orase, locuitori si asocierea de proiecte anumitor locuitori
		    afisarea tuturor acestor date; pentru fiecare oras sa se afiseze umarul total de locuitori, numarul total de proiecte si numarul de locuitori / km patrat
		    stergerea si editarea datelor existente (city,citizen,project)
		</div>
        <div>
            <a href="task_3">Link catre cerinta 3</a>
        </div>
		<hr>
		<div class="container">
			<div class="row">
                <h3>Adaugare date</h3>
				<!-- Adaugare orase -->
				<div class="col-sm-3">
					Adauga orase:
					{{Form::open(array('url' => 'create_city'))}}

					{{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Numele orasului')) }}
					{{ Form::text('state', null, array('class' => 'form-control', 'placeholder' => 'Statul')) }}
					{{ Form::text('population', null, array('class' => 'form-control', 'placeholder' => 'Numarul de locuitori')) }}
					{{ Form::text('zipcode', null, array('class' => 'form-control', 'placeholder' => 'Codul postal')) }}
					{{ Form::text('area', null, array('class' => 'form-control', 'placeholder' => 'Suprafata km2')) }}
					{{ Form::text('phone', null, array('class' => 'form-control', 'placeholder' => 'Telefon')) }}
					{{ Form::text('fax', null, array('class' => 'form-control', 'placeholder' => 'fax')) }}
					{{ Form::submit('Create city', array('class' => 'btn btn-default')) }}

					{{Form::close()}}
				</div>
				
				<!-- Adaugare locuitor -->
				<div class="col-sm-3">
					Adauga un nou locuitor: <br>
					Selecteza orasul caruia ii apartine: 
					{{Form::open(array('url' => 'create_citizen'))}}
                    <input type="button" value="Afisaza lista oraselor" class="cities_list"/>
                    <select name="city" class="selectcity">

					</select>					
					{{ Form::text('fname', null, array('class' => 'form-control', 'placeholder' => 'Prenumele')) }}
					{{ Form::text('lname', null, array('class' => 'form-control', 'placeholder' => 'Numele')) }}
					{{ Form::text('age', null, array('class' => 'form-control', 'placeholder' => 'Varsta')) }}
					{{ Form::text('address', null, array('class' => 'form-control', 'placeholder' => 'Adresa')) }}
					{{ Form::text('mobile', null, array('class' => 'form-control', 'placeholder' => 'Telefon mobil')) }}					
					{{ Form::submit('Create citizen', array('class' => 'btn btn-default')) }}				

					{{Form::close()}}
				</div>

				<!-- Adaugare proiect -->
				<div class="col-sm-3">
					Adauga un nou proiect: <br>
					Primul camp reprezinta id-ul cetateanului caruia i se atribuie proiectul:
					{{Form::open(array('url' => 'create_project'))}}		
					
					{{ Form::text('citizenID', null, array('class' => 'form-control', 'placeholder' => 'ID-ul cetateanului din DB mysql')) }}										

					{{ Form::text('projectID', null, array('class' => 'form-control', 'placeholder' => 'ID Proiect')) }}
					{{ Form::text('description', null, array('class' => 'form-control', 'placeholder' => 'Descriere')) }}
					{{ Form::text('date_start', null, array('class' => 'form-control', 'placeholder' => 'Data inceprii proiectului')) }}
					{{ Form::text('date_end', null, array('class' => 'form-control', 'placeholder' => 'Data limita a proiectului')) }}
					{{ Form::text('amount', null, array('class' => 'form-control', 'placeholder' => 'Suma')) }}					
					{{ Form::submit('Create project', array('class' => 'btn btn-default')) }}				

					{{Form::close()}}
				</div>
			</div>
            <hr/>
            <div class="row">
                <div class="col-sm-3">
                    <!--Shows the city details-->
                    <h3>Afisare date pentru oras</h3>
                    <form action="show_city_details" method="post">
                        <label for="select_city">Orasul:</label>
                        <input type="button" value="Afisaza lista oraselor" class="cities_list"/>
                        <select class="selectcity" name="select_city" id="city_selector" >
                            <?php
                            if (isset($cities_list)) {
                                foreach ($cities_list as $key => $value) {
                                    echo "<option value=\"" . $key . "\">" . $value . "</option>";
                                }
                            }
                            ?>
                        </select>
                        <input type="submit" value="Afisaza datele orasului" class="city_supp"/>
                    </form>
                    <?php
                    if(isset($number_of_citizens)) {
                        echo "Populatia orasului este: " . $number_of_citizens . " " . "locuitori. <br> ";
                    }
                    if(isset($count_projects)) {
                        echo "Numarul de proiecte din acest oras: " . $count_projects . "<br>";
                    }
                    if(isset($population_x_area)) {
                        echo "Numarul de locuitori / km2: " . $population_x_area;
                    }
                    ?>
                </div>

                <div class="col-sm-3">
                    <!--Deleting items-->
                    <div class="row">
                        <h3>Sterge orase, locuitori si proiecte</h3>

                        <!--Delete city-->
                        <form action="delete_city" method="post">
                            <label for="select_city">Orasul:</label>
                            <input type="button" value="Afisaza lista oraselor" class="cities_list"/>
                            <select class="selectcity" name="select_city" id="city_selector" >
                                <?php
                                if (isset($cities_list)) {
                                    foreach ($cities_list as $key => $value) {
                                        echo "<option value=\"" . $key . "\">" . $value . "</option>";
                                    }
                                }
                                ?>
                            </select>
                            <input type="submit" value="Sterge orasul" class="city_supp"/>
                        </form>

                        <!--Delete citizen-->
                        <form action="delete_citizen" method="post">
                            <label for="select_citizen">Locuitorul: </label>
                            <input type="button" value="Afisaza lista locuitorilor" class="citizens_list"/>
                            <select name="select_citizen" id="citizen_selector" class="selectcitizen">
                                <?php
                                if (isset($citizens_list)) {
                                    foreach ($citizens_list as $key => $value) {
                                        echo "<option value=\"" . $key[0] . "\">" . $value . "</option>";
                                    }
                                }
                                ?>
                            </select>
                            <input type="submit" value="Sterge locuitorul" class="citizens_supp"/>
                        </form>

                        <!--Delete project-->
                        <form action="delete_project" method="post">
                            <label for="select_project">ID proiect: </label>
                            <select name="select_project" id="project_selector">
                                <?php
                                if (isset($projects_list)) {
                                    foreach ($projects_list as $key => $value) {
                                        echo "<option value=\"" . $key . "\">" . $value . "</option>";
                                    }
                                }
                                ?>
                            </select>
                            <input type="submit" value="Sterge proiectul"/>
                        </form>
                    </div>
                </div>

                <div class="col-sm-3">

                    <!--Edit elements-->
                    <div class="row">
                        <h3>Editarea elementelor</h3>

                        <form action="edit_city" method="post">
                            <input type="button" value="Afisaza lista oraselor" class="cities_list"/>
                            <select class="selectcity" name="select_city_edit" id="city_selector" >
                                <?php
                                if (isset($cities_list)) {
                                    foreach ($cities_list as $key => $value) {
                                        echo "<option value=\"" . $key . "\">" . $value . "</option>";
                                    }
                                }
                                ?>
                            </select>
                            <input type="submit" value="Editeaza orasul" class="city_supp"/>
                        </form>

                        <form action="edit_citizen" method="post">
                            <select name="select_citizen_edit" id="citizen_edit_selector">
                                <?php
                                if (isset($citizens_list)) {
                                    foreach ($citizens_list as $key => $value) {
                                        echo "<option value=\"" . $key . "\">" . $value . "</option>";
                                    }
                                }
                                ?>
                            </select>
                            <input type="submit" value="Editeaza cetateanul"/>
                        </form>

                        <form action="edit_project" method="post">
                            <select name="select_project_edit" id="project_edit_selector">
                                <?php
                                if (isset($projects_list)) {
                                    foreach ($projects_list as $key => $value) {
                                        echo "<option value=\"" . $key . "\">" . $value . "</option>";
                                    }
                                }
                                ?>
                            </select>
                            <input type="submit" value="Editeaza proiectul"/>
                        </form>

                    </div>

                </div>

            </div>

            <div class="row">
                <hr/>
            </div>


        </div>
	</div>

</body>
</html>
