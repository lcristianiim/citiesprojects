@extends('master_page')

@section('content')

<div class="row">
    <script>
        $(document).ready(function(){
            $( "#create_data_btn" ).click(function() {
                $(this).addClass('hidden');
                $(".alert-warning").addClass('hidden');
                $(".to_hide").addClass('hidden');
                $(".loader").removeClass('hidden');

                $("a").click(function(){
                    var link = $(this).prop('href');

                    location.href='task_1_kill';

                    return false;
                })

                $.post('task_1_execute', ' ', function(data){
                    var result = data;
                })
                    .done(function() {
                        document.location.href='task_1';
                })
            });

        });

    </script>


    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">{{$tasks_presentation[0]['heading']}}</div>
        <div class="panel-body">
            <p>{{$tasks_presentation[0]['paragraph']}}</p>
        </div>

        <!-- List group -->
        <ul class="list-group">
            <li class="list-group-item">
<!--                href="task_1_execute"-->
                <p class="text-center" ><a id="create_data_btn" role="button" class="btn btn-default">Create random data</a></p>
                <div class="alert alert-warning">
                    <ul class="fa-ul">
                        <li>
                            <i class="fa fa-exclamation-triangle fa-li fa-lg"></i>
                            The above button will delete all existing data from the database called "<i>citiesprojects</i>" and will insert new random data. This operation may take a while. If you want to work with the existing database just go to
                            <a href="task_2" class="alert-link"> task 2</a>.
                        </li>
                    </ul>
                </div>


                <div id="loader" class="alert alert-info hidden loader">
                    <ul class="fa-ul">
                        <li class="text-center">
                            <i class="fa fa-cog fa-spin" style="font-size: 20"></i>
                            <p>Generating data...</p>
                            <p><a id="stop_button" href="task_1_kill" class="alert-link">Stop</a></p>
                        </li>
                    </ul>
                </div>

            </li>
            <li class="list-group-item to_hide">Current cities in database: {{$number_of_cities}}</li>
            <li class="list-group-item to_hide">Current citizens in database: {{$number_of_citizens}}</li>
            <li class="list-group-item to_hide">Current projects in database: {{$number_of_projects}}</li>
        </ul>
    </div>

    <script>






    </script>


</div>


@stop

@section('right_link')

@stop