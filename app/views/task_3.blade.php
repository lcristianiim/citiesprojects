<html>
<head>
	<title>CitiesProjects tasks</title>
    <script src="<?= asset('js') ?>/jquery.min.js"></script>
    <script src="<?= asset('bootstrap-master') ?>/dist/js/bootstrap.js" type="text/javascript"></script>

	<link rel="stylesheet" type="text/css" href="<?= asset('bootstrap-master') ?>/dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="">

    <style>
        table#search-results td {padding:0 10px}
        table#search-results tr:nth-child(odd) {background-color:#afd9ee}
    </style>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#search_form").submit(function(){

                $.post('ajax_post', $("#search_form").serialize(), function(data){
                    var result = JSON.parse(data);

                    if (result.error == 1) {
                        $('#content').html(result.error_message);
                    } else {
                        console.log(result.result);
                        resultHtml  =   "<table id='search-results'>";

                        for (var i = 0; i < result.result.length; i++) {

                            resultHtml +=       "<tr>";
                            resultHtml +=           "<td>Prenumele:</td><td>" + result.result[i].fname + "</td>";
                            resultHtml +=       "</tr>";
                            resultHtml +=           "<td>Numele:</td><td>" + result.result[i].lname + "</td>";
                            resultHtml +=       "</tr>";
                            resultHtml +=           "<td>Varsta:</td><td>" + result.result[i].age + "</td>";
                            resultHtml +=       "</tr>";
                            resultHtml +=           "<td>Adresa:</td><td>" + result.result[i].address + "</td>";
                            resultHtml +=       "</tr>";
                            resultHtml +=           "<td>Telefon:</td><td>" + result.result[i].mobile + "</td>";
                            resultHtml +=       "</tr>";

                        }

                        resultHtml  +=   "</table>";

                        $('#content').html(resultHtml);
                    }
                });
                return false;
            });

        });
    </script>
</head>
<body>

	<div class="container">

			<h2>Cerinta 3</h2>
		<hr>

        <div>
            <p>3. Sa se realizeze un script care sa permita cautarea unui locuitor in toate orasele existente functie de nume (lname) sau prenume (fname) sau adresa (address). Sa exista doar un singur camp HTML care sa permita introducerea expresiei de cautare. Rezultatul va fi returnat in format JSON si va fi interpretat client-side din javascript si afisate rezultatele in pagina. Este important timpul de cautare.  Se permite folosirea oricarei librarii javascript. Preferabil jQuery.</p>
		</div>

        <div>
            <a href="task_3">Link catre cerinta 4</a>
        </div>

        <hr/>

        <div class="col-sm-4">
            <form id="search_form">
                <input type="text" name="search_bar" class="form-control form-group" placeholder="Cautare dupa nume, prenume, adresa"/>
                <input type="submit" value="Cauta" class="btn btn-default"/>
            </form>
        </div>

        <div id="content">

        </div>
	</div>

</body>
</html>