<html lang="en"><head>

    <title>CitiesProjects</title>
    <script src="<?= asset('bootstrap-master') ?>/dist/js/bootstrap.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="<?= asset('bootstrap-master') ?>/dist/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?= asset('css') ?>/custom.css">
    <link rel="stylesheet" type="text/css" href="<?= asset('css') ?>/offcanvas.css">
    <script src="<?= asset('js')?>/task_2_scripts.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
    <link href="../../assets/ico/favicon.ico" rel="shortcut icon">

</head>

<body>
<div role="navigation" class="navbar navbar-fixed-top navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="/citiesprojects/public" class="navbar-brand">CitiesProjects</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class=""><a href="/citiesprojects/public">Home</a></li>
            </ul>
        </div><!-- /.nav-collapse -->
    </div><!-- /.container -->
</div><!-- /.navbar -->

<div class="container">

    <div class="row row-offcanvas row-offcanvas-right">

        <div class="col-xs-12 col-sm-9">
            <p class="pull-right visible-xs">
                <button data-toggle="offcanvas" class="btn btn-primary btn-xs" type="button">Toggle nav</button>
            </p>
            <div class="jumbotron">
                <h1>Cities projects</h1>
                <p>This is an evaluation test with 5 tasks. The main accent is on server-side programing. </p>
            </div>
            <div class="row">

                @foreach ($tasks_presentation as $task)
                <div class="col-6 col-sm-6 col-lg-4">
                    <h2>{{$task['heading']}}</h2>
                    <p>{{$task['paragraph']}}</p>
                </div><!--/span-->
                @endforeach


            </div><!--/row-->
        </div><!--/span-->

        <div role="navigation" id="sidebar" class="col-xs-6 col-sm-3 sidebar-offcanvas">
            <div class="list-group">
                @foreach ($tasks_presentation as $task)
                <a class="list-group-item" href="{{ $task['link'] }}">{{$task['heading']}}</a>
                @endforeach
            </div>
        </div><!--/span-->
    </div><!--/row-->

    <hr>

    <footer>
        <p>&copy; Company 2014</p>
    </footer>

</div><!--/.container-->



<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="../../dist/js/bootstrap.min.js"></script>
<script src="offcanvas.js"></script>


</body></html>