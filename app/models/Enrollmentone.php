<?php

use Illuminate\Support\Facades\DB;

class Enrollmentone extends Eloquent {

		/**
		 * The database table used by the model.
		 *
		 * @var string
		 */
		protected $table = 'enrollmentsone';

        public static function getAllCitizensIDbyCityID($city_id){
            $enrollments = DB::table('enrollmentsone')->where('city_id', $city_id)->get();
            $citizens_id = [];
            $citizens =[];

            foreach($enrollments as $enrollment) {
                $citizens_id[] = $enrollment->citizen_id;
//                dd($citizens_id);
            }

            foreach($citizens_id as $citizen_id) {
                $citizens[] = Citizen::find($citizen_id)->toArray();
            }

            return $citizens;
        }

}
