<?php

	class City extends Eloquent {

		/**
		 * The database table used by the model.
		 *
		 * @var string
		 */
		protected $table = 'cities';

        public static function getAreabyId($city_id){
            $aCity = DB::table('cities')->where('id', $city_id)->get();
            $area = 0;

            foreach ($aCity as $city){
                $area = $city->area;
            }

            return $area;
        }
}
