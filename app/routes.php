<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//Event::listen('illuminate.query', function($sql){
//   var_dump($sql);
//});

Route::get('testing_pdo', function()
{
    return View::make('test_laravel_speed');
});

Route::get('testing_modal', function()
{
    return View::make('testing_modal');
});

Route::get('testing_mail', function() {
    $data = [];
    Mail::send('emails.default_mail', $data, function($message){
        $message->to('lcristianiim@gmail.com')
                ->subject('Mesage from laravel');
    });
});

Route::get('/', 'HomeController@show_welcome');
Route::get('homepage', 'HomeController@show_welcome');
Route::post('send_emails', 'Task4Controller@send_emails');


Route::get('task_1', 'Task1Controller@task_1');
Route::post('task_1_execute', 'Task1Controller@task_1_execute');
Route::get('task_1_kill', 'Task1Controller@task_1_kill');

//Route::get('task_2', 'Task2Controller@task_2');
Route::get('task_2', array('as' => 'feedback', 'uses' => 'Task2Controller@task_2'));
Route::get('task_3', 'Task3Controller@task_3');
Route::get('task_4', 'Task4Controller@task_4');
Route::get('task_5', 'Task5Controller@task_5');

Route::post('show_cities', 'Task2Controller@show_cities');
Route::post('show_citizens', 'Task2Controller@show_citizens');


Route::post('ajax_post', 'Task3Controller@ajax_post');
Route::post('search_by_id', 'Task5Controller@search_by_id');

Route::post('create_city', 'Task2Controller@create_city');
Route::post('create_citizen', 'Task2Controller@create_citizen');
Route::post('create_project', 'Task2Controller@create_project');

Route::post('show_city_details', 'Task2Controller@show_city_details');

Route::post('delete_city', 'Task2Controller@delete_city');
Route::post('delete_citizen', 'Task2Controller@delete_citizen');
Route::post('delete_project', 'Task2Controller@delete_project');

Route::post('edit_city', 'Task2Controller@edit_city');
Route::post('edit_selected_city', 'Task2Controller@edit_selected_city');

Route::post('edit_citizen', 'Task2Controller@edit_citizen');
Route::post('edit_selected_citizens', 'Task2Controller@edit_selected_citizens');

Route::post('edit_project', 'Task2Controller@edit_project');
Route::post('edit_selected_project', 'Task2Controller@edit_selected_project');