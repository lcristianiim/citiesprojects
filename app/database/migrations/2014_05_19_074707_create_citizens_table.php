<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitizensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('citizens', function(Blueprint $table){
			
			$table->increments('id')->unique();
			
			$table->string('fname');
			$table->string('lname');			
			$table->integer('age');
			$table->string('address');
			$table->string('mobile');
            $table->string('email');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('citizens');
	}

}
