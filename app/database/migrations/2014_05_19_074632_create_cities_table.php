<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cities', function(Blueprint $table){
			
			$table->increments('id')->unique();
			
			$table->string('name');
			$table->char('state');
			$table->integer('population');
			$table->string('zipcode');
			$table->integer('area');
			$table->string('phone');
			$table->string('fax');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cities');
	}

}
