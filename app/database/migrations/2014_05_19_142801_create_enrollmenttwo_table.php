<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnrollmenttwoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('enrollmentstwo', function(Blueprint $table){
			
			$table->increments('id')->unique();

			$table->integer('citizen_id');
			$table->integer('project_id');

			$table->timestamps();

            $table->index('citizen_id');
		});//
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('enrollmentstwo');
	}

}
