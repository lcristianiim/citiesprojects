<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnrollmentoneTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('enrollmentsone', function(Blueprint $table){
			
			$table->increments('id')->unique();

			$table->integer('city_id');
			$table->integer('citizen_id');			

			$table->timestamps();

            $table->index('citizen_id');
		});//
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('enrollmentsone');
	}

}
