/**
 * Created by Cristian on 6/9/14.
 */
function light_current_links() {
    $(document).ready(function(){
        var pathname = window.location.pathname;
        var homepage = '/citiesprojects/public/';
        var task_1 = '/citiesprojects/public/task_1';
        var task_2 = '/citiesprojects/public/task_2';
        var task_3 = '/citiesprojects/public/task_3';
        var task_4 = '/citiesprojects/public/task_4';
        var task_5 = '/citiesprojects/public/task_5';

        if (pathname == homepage){
            $("#home_butt").addClass('active');
        }

        for(i=1; i<6; i++){
            if (pathname == '/citiesprojects/public/task_' + i){
                $("#task_"+i+"_butt").addClass('active');
            }
        }
    })
}



function city_details(){
    $(document).ready(function(){
        $("#city_supp_butt").click(function(){

        });
    })
}

function form_city_details(){
    $(document).ready(function(){
        $("#form_city_details").submit(function(){
            $.post('show_city_details', $( "#form_city_details" ).serialize(), function(data){
                var result = JSON.parse(data);
                console.log(result);
                resultHtml = "<div class='row-fluid'>";


                resultHtml += "<strong>City:</strong> " + result.name_of_city + "</br>";
                resultHtml += "<strong>Population:</strong> " + result.number_of_citizens + "</br>";
                resultHtml += "<strong>Projects:</strong> " + result.number_of_projects + "</br>";
                resultHtml += "<strong>Population per km<sup>2</sup>: </strong>" + result.population_x_area + "</br>";

                resultHtml += "</div>"

                $("#city_data").html(resultHtml);
                $('#to_show_city_data').removeClass('hidden');

            })
            return false;
        })
    })
}

// var_1 = #form_search_citizen
// var_2 = #show_result_2
// var_3 = #show_loading
// var_4 = #div_delete_citizen
// var_5 = #show_result


function search_for_citizen(var_1, var_2, var_3, var_4, var_5){
    $(document).ready(function(){
        $(var_1).submit(function(){
            $(var_2).addClass('hidden');
            $(var_3).removeClass('hidden');
            $(var_3).html('Loading..');
            $.post('ajax_post', $(var_1).serialize(), function(data){
                var result = JSON.parse(data);
                console.log(result.error);

                resultHtml = "<div class='form-group'>";

                if (result.error == 0) {
                    $(var_4).removeClass('hidden');
                    resultHtml += "<select name='select_citizen' id='citizen_selector' class='selectcitizen form-control'>";

                    $.each(result.result, function (key, value) {
                        resultHtml += "<option value= \"" + value.id +"\" > " + value.fname + "</option>";
                    });
                    resultHtml += "</select>";

                    resultHtml += "</div>"
                    $(var_3).addClass('hidden');
                    $(var_2).addClass('hidden');
                    $(var_5).html(resultHtml);

                } else {
                    resultHtml += 'Nu au fost gasite inregistrari';
                    $(var_3).addClass('hidden');
                    $(var_4).addClass('hidden');
                    $(var_2).html(resultHtml);
                    $(var_2).removeClass('hidden');
                    resultHtml += "</div>"
                }
            })
            return false;
        })
    })
}

function modal_1(){
    $(document).ready(function(){
        $('#input_form_edit_city').click(function(){
            var city_id = $('#city_selector_edit_city option:selected').val();
            $.post('edit_city', $('#form_edit_city').serialize(), function(data){
                result = JSON.parse(data);
                console.log(result);

                resultHtml = "<form id='form_edit_selected_city' action='edit_selected_city' method='POST'>";

                resultHtml += "<div class='form-group hidden'>";
                resultHtml += "<label for='city_id'>City id:</label>";
                resultHtml += "<input type='text' name='city_id' class='form-control' value='" + result.city_id + "'>";
                resultHtml += "</div>";

                resultHtml += "<div class='form-group'>";
                resultHtml += "<label for='name'>Numele orasului:</label>";
                resultHtml += "<input type='text' name='name' class='form-control' value='" + result.city_name + "'>";
                resultHtml += "</div>";

                resultHtml += "<div class='form-group'>";
                resultHtml += "<label for='state'>Judetul:</label>";
                resultHtml += "<input type='text' name='state' class='form-control' value='" + result.city_state + "'>";
                resultHtml += "</div>";

                resultHtml += "<div class='form-group'>";
                resultHtml += "<label for='population'>Numar de locuitori:</label>";
                resultHtml += "<input type='text' name='population' class='form-control' value='" + result.city_population + "'>";
                resultHtml += "</div>";

                resultHtml += "<div class='form-group'>";
                resultHtml += "<label for='zipcode'>Cod postal:</label>";
                resultHtml += "<input type='text' name='zipcode' class='form-control' value='" + result.city_zipcode + "'>";
                resultHtml += "</div>";

                resultHtml += "<div class='form-group'>";
                resultHtml += "<label for='area'>Suprafata in km<sup>2</sup>:</label>";
                resultHtml += "<input type='text' name='area' class='form-control' value='" + result.city_area + "'>";
                resultHtml += "</div>";

                resultHtml += "<div class='form-group'>";
                resultHtml += "<label for='phone'>Telefon:</label>";
                resultHtml += "<input type='text' name='phone' class='form-control' value='" + result.city_phone + "'>";
                resultHtml += "</div>";

                resultHtml += "<div class='form-group'>";
                resultHtml += "<label for='fax'>Fax:</label>";
                resultHtml += "<input type='text' name='fax' class='form-control' value='" + result.city_fax + "'>";
                resultHtml += "</div>";

                resultHtml += "</form>";

                $('#submit_edit_city').click(function(){
                    $.post('edit_selected_city', $('#form_edit_selected_city').serialize(), function(data){
                        location.reload()
                    })
                })

                $('#modal_1_result').html(resultHtml);
            })
        })
    })
}

function modal_2(){
    $(document).ready(function(){
        $('#form_edit_citizen').submit(function(){
            $.post('edit_citizen', $('#form_edit_citizen').serialize(), function(data){
                var result = JSON.parse(data);
                console.log(result);

                resultHtml = "<form id='form_edit_selected_citizen' action='edit_selected_citizen' method='POST'>";

                resultHtml += "<div class='form-group hidden'>";
                resultHtml += "<label for='citizen_id'>Prenume:</label>";
                resultHtml += "<input type='text' name='citizen_id' class='form-control' value='" + result.citizen_id + "'>";
                resultHtml += "</div>";

                resultHtml += "<div class='form-group'>";
                resultHtml += "<label for='fname'>Prenume:</label>";
                resultHtml += "<input type='text' name='fname' class='form-control' value='" + result.citizen_fname + "'>";
                resultHtml += "</div>";

                resultHtml += "<div class='form-group'>";
                resultHtml += "<label for='lname'>Nume:</label>";
                resultHtml += "<input type='text' name='lname' class='form-control' value='" + result.citizen_lname + "'>";
                resultHtml += "</div>";

                resultHtml += "<div class='form-group'>";
                resultHtml += "<label for='age'>Varsta:</label>";
                resultHtml += "<input type='text' name='age' class='form-control' value='" + result.citizen_age + "'>";
                resultHtml += "</div>";

                resultHtml += "<div class='form-group'>";
                resultHtml += "<label for='address'>Adresa:</label>";
                resultHtml += "<input type='text' name='address' class='form-control' value='" + result.citizen_address + "'>";
                resultHtml += "</div>";

                resultHtml += "<div class='form-group'>";
                resultHtml += "<label for='mobile'>Telefon:</label>";
                resultHtml += "<input type='text' name='mobile' class='form-control' value='" + result.citizen_mobile + "'>";
                resultHtml += "</div>";

                resultHtml += "<div class='form-group'>";
                resultHtml += "<label for='email'>Email:</label>";
                resultHtml += "<input type='text' name='email' class='form-control' value='" + result.citizen_email + "'>";
                resultHtml += "</div>";

                resultHtml += "</form>";

                $('#submit_edit_citizen').click(function(){
                    $.post('edit_selected_citizens', $('#form_edit_selected_citizen').serialize(), function(data){
                        location.reload();
                    })
                })

                $('#modal_2_result_second').html(resultHtml);
            })
            return false;
        })
    })
}
function modal_3(){
    $(document).ready(function(){
        $('#form_edit_project').submit(function(){
            $.post('edit_project', $('#form_edit_project').serialize(), function(data){
                result = JSON.parse(data);

                resultHtml = "<form id='form_edit_selected_project' action='edit_selected_project' method='POST'>";

                resultHtml += "<div class='form-group hidden'>";
                resultHtml += "<label for='project_id'>Database id:</label>";
                resultHtml += "<input type='text' name='project_id' class='form-control' value='" + result.project_id + "'>";
                resultHtml += "</div>";

                resultHtml += "<div class='form-group'>";
                resultHtml += "<label for='projectID'>Project ID:</label>";
                resultHtml += "<input type='text' name='projectID' class='form-control' value='" + result.projectID + "'>";
                resultHtml += "</div>";

                resultHtml += "<div class='form-group'>";
                resultHtml += "<label for='description'>Descriere:</label>";
                resultHtml += "<textarea type='text' name='description' class='form-control' rows='5' >";
                resultHtml += result.description;
                resultHtml += "</textarea>";
                resultHtml += "</div>";

                resultHtml += "<div class='form-group'>";
                resultHtml += "<label for='amount'>Cantitate:</label>";
                resultHtml += "<input type='text' name='amount' class='form-control' value='" + result.amount + "'>";
                resultHtml += "</div>";

                resultHtml += "<div class='form-group'>";
                resultHtml += "<label for='date_start'>Inceputul proiectului:</label>";
                resultHtml += "<input type='text' name='date_start' class='form-control' value='" + result.date_start + "'>";
                resultHtml += "</div>";

                resultHtml += "<div class='form-group'>";
                resultHtml += "<label for='date_end'>Sfarsitul proiectului:</label>";
                resultHtml += "<input type='text' name='date_end' class='form-control' value='" + result.date_end + "'>";
                resultHtml += "</div>";

                resultHtml += "</form>";

                $('#modal_1_result_third').html(resultHtml);

                $('#submit_edit_project').click(function(){
                    $.post('edit_selected_project', $('#form_edit_selected_project').serialize(), function(data){
                        location.reload();
                    })
                })


            })
            return false;
        })
    })
}
function send_emails_form(){
    $(document).ready(function(){
        $('#button_send_emails').click(function(){

            loadingHtml = "<div class='alert alert-info text-center'>";
            loadingHtml +=    "<i class='fa fa-cog fa-spin' style='font-size: 20'></i>";
            loadingHtml +=  "<p>Se trimit mesajele...</p>";
            loadingHtml += "</div>";

            $('#status').removeClass('hidden');
            $('#status').html(loadingHtml);

            $.post('send_emails', '', function(data){
                result = JSON.parse(data);
                console.log(result.number_of_emails);

                if (result.number_of_emails == 0) {
                    succesfullHtml = "<div class='alert alert-success text-center'>";
                    succesfullHtml +=  "<strong>" + result.number_of_emails + " " + "Mesaje trimise!</strong>";

                    succesfullHtml += "<div>"
                    succesfullHtml += "<p>Nu exista proiecte expirate.</p>";
                    succesfullHtml += "</div>"

                    succesfullHtml += "</div>";
                }

                if (result.number_of_emails == 1) {

                    succesfullHtml = "<div class='alert alert-success text-center'>";
                    succesfullHtml +=  "<strong>" + result.number_of_emails + " " + "Mesaj trimis!</strong>";

                    succesfullHtml += "<div>"
                    succesfullHtml += "<p>Mesajul a fost trimis cu succes sub forma de email.</p>";
                    succesfullHtml += "</div>"

                    succesfullHtml += "</div>";
                }

                if (result.number_of_emails > 1) {

                    succesfullHtml = "<div class='alert alert-success text-center'>";
                    succesfullHtml +=  "<strong>" + result.number_of_emails + " " + "Mesaje trimise!</strong>";

                    succesfullHtml += "<div>"
                    succesfullHtml += "<p>Mesajele au fost trimise cu succes sub forma de email.</p>";
                    succesfullHtml += "</div>"

                    succesfullHtml += "</div>";
                }



                $('#status').html(succesfullHtml);


            })
        })
    })
}

